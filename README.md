# Booj Books - Front End Code Test

This code test is for candidates applying for front-end positions with booj/REMAX.

## Requirements

- Node.js - v12 or greater
- NPM
- Code Editor - (most of us use VS Code)

## Run Front End Locally

Clone the project

```bash
  git clone https://gitlab.com/tylerpetz/book-test.git
```

Go to the project directory

```bash
  cd book-test
```

Install dependencies

```bash
  npm install
```

Serve with hot reload at localhost:3000

```bash
  npm run dev
```

## Tasks

Your tasks will be waiting for you at <http://localhost:3000>
