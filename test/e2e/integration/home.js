describe('Sample tests', () => {
  it('Visits index page', () => {
    cy.visit('/');
    cy.contains('[data-test="site-logo"]', 'booj books');
  });
});
