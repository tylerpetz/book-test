const randomNameGenerator = () => {
  let res = '';
  for(let i = 0; i < 10; i++){
     const random = Math.floor(Math.random() * 27);
     res += String.fromCharCode(97 + random);
  };
  return res;
};

describe('Booj Books - Code Test', () => {
  it('Visits homepage', () => {
    cy.visit('/');
    cy.contains('[data-test="site-logo"]', 'booj books');
    cy.wait(500); // wait for cookies to parse
    cy.get('[data-test="auth-menu"]').then(($menu) => {
      // log out if old user is hanging
      if ($menu.text().includes('Log Out')) {
        cy.get('[data-test="logout-link"]').click()
      }
      cy.get('[data-test="register-link"]').click()
    })
  });

  it('Can navigate to registration page', () => {
    cy.get('[data-test="register-link"]').click()
    cy.url().should('include', '/register')
  });

  it('Can register a new user', () => {
    const user = randomNameGenerator()
    cy.get('[data-test="user-name"]').type(user)
    cy.get('[data-test="user-email"]').type(user + '@gmail.com')
    cy.get('[data-test="user-password"]').type(user)
    cy.get('[data-test="user-password-repeat"]').type(user)
    cy.get('[data-test="user-submit"]').click()
    cy.url().should('eq', Cypress.config().baseUrl + '/')
    cy.contains('#toasts', user)
  });

  it('Can search for Harry Potter', () => {
    cy.get('[data-test="search-link"]').click()
    cy.url().should('include', '/search')
    cy.get('[data-test="book-search-input"]').type('Harry Potter')
    cy.get('[data-test="book-search-submit-button"]').click()
    cy.get('[data-test="search-results"]').should('be.visible')
  });

  it('Can view info on first book', () => {
    cy.get('[data-test="book-title"]').first().then(($title) => {
      let bookTitle = ''
      bookTitle = $title[0].textContent.trim()
      cy.get('[data-test="more-info"]').first().click()
      cy.contains('[data-test="book-modal"] [data-test="book-title"]', bookTitle)
    })
    cy.get('[data-test="close-modal"]').click()
    cy.get('[data-test="modal-content"]').should('not.exist')
  });

  it('Add the first book to the first list', () => {
    cy.get('[data-test="add-to-my-books"]').first().click()
  });

  it('Can log user out', () => {
    cy.get('[data-test="logout-link"]').click()
  });
});
