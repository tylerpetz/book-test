module.exports = {
  client: {
    includes: ['./**/*.gql'],
    service: {
      url: "https://sheltered-headland-93675.herokuapp.com/graphql",
    }
  }
};
