import { createHttpLink } from 'apollo-link-http'
import fetch from 'isomorphic-fetch'

export default function({ app }) {
  return {
    defaultHttpLink: false,
    link: createHttpLink({
      uri: 'http://localhost:3000/laravel/graphql',
      credentials: 'include',
      query: {
        fetchPolicy: 'no-cache',
      },
      fetch: (uri, options) => {
        const token = app.$cookies.get('XSRF-TOKEN')
        options.headers['X-XSRF-TOKEN'] = token
        return fetch(uri, options)
      }
    })
  }
}
