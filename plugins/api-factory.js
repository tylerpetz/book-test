export default (context, inject) => {
  const apiClient = context.$axios.create({
    withCredentials: true,
  })

  apiClient.interceptors.response.use(
    (response) => response,
    (error) => {
      if (
        error.response &&
        [401, 419].includes(error.response.status) &&
        context.store.getters["loggedIn"]
      ) {
        context.store.dispatch("logout")
      }
      return Promise.reject(error);
    }
  );

  const delay = ms => new Promise(res => setTimeout(res, ms))

  const apiRequests = {
    auth: {
      getCookie() {
        return apiClient.get("/laravel/sanctum/csrf-cookie");
      },

      register(payload) {
        return apiClient.post("/laravel/register", payload);
      },

      login(payload) {
        return apiClient.post("/laravel/login", payload);
      },

      logout() {
        return apiClient.post("/laravel/logout");
      },

      getUser() {
        return apiClient.$get("/laravel/api/user");
      },
    },
    books: {
      async searchBooks(searchQuery) {
        await delay(2000)
        return apiClient.$get(`/book-search?s=${searchQuery}`)
      }
    },
  };
  inject("api", apiRequests);

  const handleError = (e) => {
    let errorMessage = 'There was an error'
    const errors = e.response && e.response.data && e.response.data.errors && e.response.data.errors ? e.response.data.errors : ''
    if (errors) {
      errorMessage = Object.values(errors).join('<br/>')
    }
    context.$toast.show({
      type: 'error',
      title: 'Whoops',
      message: errorMessage,
    })
  }

  inject("handleError", handleError);
};
