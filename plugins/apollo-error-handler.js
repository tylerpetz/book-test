export default ({ graphQLErrors }, { redirect }) => {
  if (graphQLErrors.some(e => e.message === 'Unauthenticated.')) {
    redirect('/')
  }
}
