export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'booj-books',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/api-factory' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://composition-api.nuxtjs.org/
    '@nuxtjs/composition-api/module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/apollo',
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
    '@nuxtjs/proxy',
    '@nuxtjs/svg-sprite',
    // https://github.com/acidjazz/tv-toast
    ['nuxt-tailvue',
      {
        toast: {
          defaults: {
            containerClasses: ['mt-12']
          },
          defaultProps: {
            timeout: 20,
          }
        }
      }
    ]
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    credentials: true,
    proxy: true
  },

  // https://axios.nuxtjs.org/options/#proxy
  proxy: {
    // proxy for google books api
    '/book-search': {
      target: 'https://www.googleapis.com/books/v1/volumes?key=AIzaSyBJ4FU3dYRf-cXgrk33PWYr-ir0yhhPxLc&q=',
      pathRewrite: { '^/book-search/?s=': '' },
    },
    // proxy for laravel backend - running locally
    '/laravel': {
      target: 'https://sheltered-headland-93675.herokuapp.com',
      pathRewrite: { '^/laravel': '/' }
    },
  },

  // Apollo module configuration: https://github.com/nuxt-community/apollo-module
  apollo: {
    clientConfigs: {
      default: '~/apollo-client.config'
    },
    defaultOptions: {
      $query: {
        fetchPolicy: 'no-cache',
      },
    },
    errorHandler: '~/plugins/apollo-error-handler.js',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    }
  },
}
