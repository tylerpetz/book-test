export const state = () => ({
  user: null,
})

export const getters = {
  loggedIn: (state) => state.user ? true : false
}

export const actions = {
  nuxtServerInit() {
    this.$api.auth.getCookie()
  },
  async register({ commit }, user) {
    try {
      await this.$api.auth.register(user)
      const newUser = await this.$api.auth.getUser()
      commit("SET_USER", newUser)
      this.$toast.show({
        type: 'success',
        title: 'Success',
        message: `Thanks for registering, ${newUser.name}!`,
      })
      this.$router.push('/')
    } catch (e) {
      this.$handleError(e)
    }
  },
  async login({ commit }, user) {
    try {
      await this.$api.auth.login(user)
      const newUser = await this.$api.auth.getUser()
      commit("SET_USER", newUser)
      this.$toast.show({
        type: 'success',
        title: 'Success',
        message: `Welcome back, ${newUser.name}!`,
      })
      this.$router.push('/')
    } catch (e) {
      this.$handleError(e)
    }
  },
  async logout({ commit }) {
    try {
      await this.$api.auth.logout()
      commit("SET_USER")
      this.$toast.show({
        type: 'success',
        title: 'Success',
        message: 'See ya next time',
      })
      this.$router.push('/')
    } catch (e) {
      this.$handleError(e)
    }
  },
}

export const mutations = {
  SET_USER(state, user = null) {
    state.user = user
  }
}
